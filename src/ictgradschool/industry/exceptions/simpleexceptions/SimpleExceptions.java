package ictgradschool.industry.exceptions.simpleexceptions;

import java.util.Scanner;


public class SimpleExceptions {
    public static void main(String[] args) {
        SimpleExceptions exceptions = new SimpleExceptions();

        //Question 1 & 2
        try {
            exceptions.handlingException();
        } catch (NumberFormatException e) {
            System.out.println("Please only enter numbers");
        }


        //Question3
        try {
            exceptions.Question3();
        } catch (StringIndexOutOfBoundsException e){
            System.out.println(e.getMessage());
        }

        //Question4
        try {
            exceptions.Question4();
        } catch (ArrayIndexOutOfBoundsException e){
            System.out.println("the array index was out of bounds");
        }
    }

    /**
     * The following tries to divide using two user input numbers, but is
     * prone to error.
     */
    public void handlingException() throws NumberFormatException {
        Scanner sc = new Scanner(System.in);


        System.out.print("Enter the first number: ");
        String str1 = sc.next();
       int num1 = Integer.parseInt(str1);
        System.out.print("Enter the second number: ");
        String str2 = sc.next();
        int num2 = Integer.parseInt(str2);

        // Output the result
        try {
            System.out.println("The division of " + num1 + " over " + num2 + " is " + (num1 / num2) + "\n");
        } catch(ArithmeticException e) {
            System.out.println("Tried to divide by 0");
        }


    }

    public void Question3() throws StringIndexOutOfBoundsException {
        String s = "hello";
        System.out.println(s.charAt(7));
    }

    public void Question4() throws ArrayIndexOutOfBoundsException {
        int[] array = new int[3];
        int x = array[4];
        System.out.println(x);
        //Write some Java code which throws a ArrayIndexOutOfBoundsException
    }
}