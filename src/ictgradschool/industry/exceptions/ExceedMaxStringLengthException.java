package ictgradschool.industry.exceptions;

public class ExceedMaxStringLengthException extends Exception {
    public ExceedMaxStringLengthException() {
    }

    public ExceedMaxStringLengthException(String message) {
        super(message);
    }

    public ExceedMaxStringLengthException(Throwable cause) {
        super(cause);
    }

    public ExceedMaxStringLengthException(String message, Throwable cause) {
        super(message, cause);
    }


}
