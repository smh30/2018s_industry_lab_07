package ictgradschool.industry.exceptions;

import ictgradschool.Keyboard;

public class WordInitials {

    public void start() {
        System.out.println("Enter a string of at most 100 characters: ");
        try {
            String [] words = getString();
            System.out.println("You entered: " + getInitials(words));

        } catch (ExceedMaxStringLengthException | InvalidWordException e) {
            System.out.println(e.getMessage());
        }


    }

    public String[] getString() throws ExceedMaxStringLengthException, InvalidWordException {

        String string = Keyboard.readInput();
        if (string.length() > 100) {
            throw new ExceedMaxStringLengthException("Your string is too long");
        }
        String[] words = breakString(string);
        for (int i = 0; i < words.length ; i++) {
            if (Character.isDigit(words[i].charAt(0))){
                throw new InvalidWordException("One or more of your words is invalid");
            }
        }
        return words;
    }

    public String[] breakString(String input){
        int numberOfWords = numberOfSpaces(input)+1;
        String[] words = new String[numberOfWords];
        int i =0;
        while (numberOfWords >1){
            words[i] = input.substring(0, input.indexOf(' '));
            input = input.substring(input.indexOf(' ')+1);
            i++;
            numberOfWords--;
        }
        words[i]=input;
        return words;
    }



    public int numberOfSpaces(String input) {
        // TODO write statements here
        char targetChar = ' ';
        int numberOfSpaces = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == targetChar) {
                numberOfSpaces++;
            }
        }

        return numberOfSpaces;
    }

    public String getInitials(String[] words){
        String initials = "";
        for (int i = 0; i < words.length ; i++) {
            initials += words[i].charAt(0) + " ";
        }
        return initials;
    }






    public static void main(String[] args) {
        new WordInitials().start();
    }
}
