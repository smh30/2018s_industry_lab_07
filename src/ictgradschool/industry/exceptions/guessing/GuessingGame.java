package ictgradschool.industry.exceptions.guessing;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    /**
     * Plays the actual guessing game.
     *
     * You shouldn't need to edit this method for this exercise.
     */
    public void start() {

        int goal = (int)(Math.random()*100 + 1);
        int guess = 0;

        while(guess != goal){
            guess = getUserGuess();
            if (guess > goal) {
                System.out.println("Too high, try again");
            }
            else if (guess < goal) {
                System.out.println("Too low, try again");
            }
            else{
                System.out.println("Perfect!");
            }
        }
        System.out.println("Goodbye");

    }

    /**
     * Gets a random integer between 1 and 100.
     *
     * You shouldn't need to edit this method for this exercise.
     */


    /**
     * Gets the user's guess from the keyboard. Currently assumes that the user will always enter a valid guess.
     *
     * TODO Implement some error handling, for the cases where the user enters a value that's too big, too small, or
     * TODO not an integer. Change this method so it's guaranteed to return an integer between 1 & 100, inclusive.
     */
    private int getUserGuess() {
        int input = 0;
        while(input == 0){
            System.out.print("Enter your guess (1-100): ");
            try {
                input = Integer.parseInt(Keyboard.readInput());

                if (input <1 || input >100){
                    System.out.println("Outside of valid range - please enter a number between 1 & 100.");
                    input = 0;
                }
            } catch (NumberFormatException e) {
                System.out.println("Please only enter numbers.");
            }
        }
        return input;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
